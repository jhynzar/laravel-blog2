<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');
Route::get('/posts/create', 'PostController@create');
Route::post('/posts','PostController@store');
Route::get('/posts/{post}', 'PostController@show');


/*
posts 						Analogy - Resource

GET /posts 					View Posts
GET /posts/create 			Create Posts
POST /posts 				Send the created post to database
GET /posts/{id}/edit 		Edit post
GET /posts/{id} 			View SPECIFIC post   		---\
PATCH /posts/{id} 			Save edited to database 	-------> Same URIs different requests [NOTE]
DELETE /posts/{id} 			Delete SPECIFIC post 		---/

*/
