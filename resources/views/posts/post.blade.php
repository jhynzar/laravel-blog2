<div class="blog-post">
  <h2 class="blog-post-title">
    <a href="/posts/{{ $post->id }}">
      {{ $post->title }}
    </a>
  </h2>
  <p class="blog-post-meta"> 
    {{$post->created_at->toFormattedDateString()}} <!-- timestamps are automatically an instance of Carbon (A Library) -->
  </p>

  {{$post->body}}
</div><!-- /.blog-post -->